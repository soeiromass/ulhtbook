<html>
<head>
    <title> Registo -ULHTBOOK </title>
    <link rel='stylesheet' href ='style.css' />
</head>

<body>
<?php include 'connect.php'; ?>
<?php include 'functions.php'; ?>
<?php include 'header.php';  ?>

<div class='container'>
    <h3>Login - ULHTBOOK</h3>
    <form method='post'>
        <?php
            if(isset($_POST['submit'])){
                $username= $_POST['username'];
                $password= $_POST['password'];

                if (empty($username) || empty($password)) {
                    if(empty($username) && empty($password)){
                        $mensage= "Intruduzir Credenciais!";
                    }
                    if(empty($username) && !empty($password)){
                        $mensage= "Intruduzir o Nome do Utilizador!";
                    }
                    if(!empty($username) && empty($password)){
                        $mensage= "Intruduzir a Palavra Chave!";
                    }
                }
                else{
                    $loginQuery = $pdo->prepare("SELECT COUNT(`ID`) FROM `users` WHERE `username`=':username' AND `password`=':password'");  
                    $IDQuery= $pdo->prepare("SELECT ID FROM `users` WHERE `username`=':username' AND `password`=':password'");                  
                    $loginQuery->bindParam(':username', $username);
                    $loginQuery->bindParam(':password', $password);
                    $IDQuery->bindParam(':username', $username);
                    $IDQuery->bindParam(':password', $password);
                    $loginQuery->execute();
                    $IDQuery->execute();
                    $count = $loginQuery->fetchColumn();        
                    if($count == "1"){
                        $get = $IDQuery->fetch(PDO::FETCH_BOTH);
                        $uid= $get['ID'];
                        echo "o id é: ".$uid;
                        $_SESSION['uid'] = $uid;
                        
                        header('location: index.php');

                        $mensage="O Login feito com Sucesso. DONE!";
                        echo $mensage;   
                    }
                    else{
                        $mensage="O Nome do Utilizador ou Palavra Chave, estão Incorrectos!";
                    }
                }
                echo "<div class='box'>$mensage</div>";
            }
        ?>
        Nome do Utilizador:<br>
        <input type='text' name='username'/><br>        
        Palavra Chave: <br>
        <input type='password' name='password'/><br>
        <input type='submit' name='submit' value='Login'/>
    </form>
</div>
</body>
</html>