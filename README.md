# ULHTBOOK

### Base de Dados
Utilizamos uma base de dados com um mecanismo de armazenamento InnoDB, devido a que pensamos em utilizar uma base de dados com relações.
Entretanto com o desenrolar do projeto verificamos que seria mais simples e não seria necessário ter relações entre os dados na Base de Dados.
Preferimos em vez disso, utilizar uma tabela que guardasse todos os pedidos de amizade.

Na nossa base de dados temos 3 tabelas:
- friendrequest: Esta tabela guarda os pedidos de amizade feitos pelos utilizadores, guarda apenas os ID's
- friendslist: Esta tabela guarda os ID's dos users que são amigos.
- users: Esta tabela guarda todos os dados de cada utilizador.

### PHP
As query que são executadas nos ficheiros de php, são feitos por PDO, evitando assim alguns problemas de SQL injection, em que foi utilizado a função `bindParam()`para esse fim.
- connect.php: Este ficheiro serve para fazer a conexão com a Base de Dados;
- disconnect.php: Este ficheiro não é utilizado, apenas serve para o caso de ser preciso colocar a variável $pdo a NULL. 
- edit.php: Este ficheiro serve para editar os dados dos utilizadores, em que apenas cada user pode editar o seu perfil e o Admin pode editar de todos. Neste ficheiro o Admin tambem recebe um request para apagar users.
- friends.php: Este ficheiro verifica na base de dados se o ID do utilizador tem ou não pedidos de amizade.
- friendrequest.php: Este ficheiro recebe os requests envidos pelo profile.php relativamente a ser aos pedidos de amizade.
- functions.php: Este ficheiro tem funções que podem ser chamadas várias vezes por diferentes outros ficheiros, é boa pratica não utilizar código repetido.
- header.php: Este ficheiro contem a Topbar da página com as diferentes páginas que podem ser acedidas, e distingue o tipo de utilizadores que as podem aceder, mostrando mais ou menos opções.
- index.php: Este ficheiro contem a página inicial, que apenas tem os dados do grupo, e das pessoas que fizeram o projeto.
- login.php: Este ficheiro corresponde a página de login, onde o login pode ser feito e submetido, iniciando a sessão.
- logout.php: Este ficheiro corresponde ao logout, em que faz a destruição da sessão.
- memberListGuest.php: Este ficheiro contem a lista de utilizadores que alguém não logado poderá aceder.
- members.php: Este ficheiro tem todos a lista de todos os utilizadores e sempre que se clica num utilizador pode-se aceder á pagina desse utilizador pelo ID no URL.
- profile.php: Este ficheiro mostra todas as opções possíveis de se fazer em cada perfil, e distingue os diferentes níveis de permissão, distinguindo os diferentes users.
- profileForGuests.php: Este ficheiro contem a informação que os Guests podem aceder, não estando logados.
- register.php: Este ficheiro contém o formulário de registo, para se poder registar por um novo utilizador, apenas pode ser acedido pelo Admin.
- request.php: Este ficheiro verifica se um utilizador tem novos pedidos de amizade ou não e coloca os nomes dos novos pedidos de amizade.
- style.css: Este ficheiro apenas contém toda a parte de estilo da página, é um ficheiro CSS.
- ulhtbook.sql: Este ficheiro tem todas as informações sobre a nossa base de dados, este ficheiro foi exportado do PHPmyAdmin.

Os ficheiro `connect.php`, `functions.php` e `header.php` são passados por include para todas as páginas que são visíveis pelos diferentes users, devido a serem necessários em todas elas.

### Convenção:
A convenção que utilizamos para cada variável for o "camelCase" devido ao grupo estar mais habituado a utilizar esse tipo de convenção, uma vez que estamos
mais habituados a programar em linguagens como o JAVA em que é mais habitual utilizar "camelCase".
Colocamos por convenção que o Admin tem o `ID = 1`, isto significa que sempre que existir condições como `if( $myID == '1' )`, significa que apenas o Admin pode aceder a essa parte.
Sempre que nos referimos ao `ID` colocamos sempre o 'I' e o 'D' em maiúsculas devido a ser um dado relevante e chama mais atenção desta forma.

### Interação entre o PHP e a Base de Dados
Algumas interações que fizemos entre o PHP e a Base de Dados foram feitas de modo a poder não utilizar Base de Dados Relacional, conseguindo assim diminuir a complexidade do projeto.
Em exemplo: `SELECT firstUser,secondUser FROM friendslist WHERE (firstUser=':myID' OR secondUser=':myID')`
Esta query verificamos caso o user seja o firstUser ou o secondUser na base de dados, ele seleciona esse user, evitando assim que se tivesse de colocar duas vezes os mesmos dados mas
de formas diferentes `(1 - firstUser e 2 - secondUser)` e postriormente `(2 - no firstUser e 1- secondUser)`, colocando apenas de uma dessas duas formas e verificar qual das formas foi colocada.

### Trabalho Realizado por:
André Cerveira, Nº21402048
João Batista, Nº21404080
Miguel Soeiro, Nº21502788