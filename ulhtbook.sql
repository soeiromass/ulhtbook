-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 14-Dez-2016 às 23:12
-- Versão do servidor: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ulhtbook`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `friendrequest`
--

CREATE TABLE `friendrequest` (
  `ID` int(11) NOT NULL,
  `from` int(11) NOT NULL,
  `to` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `friendrequest`
--

INSERT INTO `friendrequest` (`ID`, `from`, `to`) VALUES
(7, 6, 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `friendslist`
--

CREATE TABLE `friendslist` (
  `ID` int(11) NOT NULL,
  `firstUser` int(11) NOT NULL,
  `secondUser` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `friendslist`
--

INSERT INTO `friendslist` (`ID`, `firstUser`, `secondUser`) VALUES
(1, 1, 2),
(2, 3, 1),
(9, 1, 4),
(8, 1, 6),
(10, 7, 10),
(11, 3, 10),
(12, 3, 11);

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE `users` (
  `ID` int(11) NOT NULL,
  `username` text NOT NULL,
  `password` text NOT NULL,
  `name` text NOT NULL,
  `profilePic` text NOT NULL,
  `age` text NOT NULL,
  `professionalExperience` text NOT NULL,
  `hobbies` text NOT NULL,
  `volunteerExperience` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`ID`, `username`, `password`, `name`, `profilePic`, `age`, `professionalExperience`, `hobbies`, `volunteerExperience`) VALUES
(1, 'Admin', 'adm123', 'Cosme Damião', 'http://1.bp.blogspot.com/-rDl_bS0usMw/TrBmh6kHkiI/AAAAAAAAAsw/ljIKlx00uug/s640/Cosme+Dami%25C3%25A3o_.jpg', '131', 'Jogador do Sport Lisboa e Benfica, e Fundador do Melhor Clube do Mundo', 'Ser o melhor da Segunda Circular', 'Deixar perder um capeonatozito para o SCP'),
(2, 'mass', 'mass', 'Miguel Soeiro', 'https://media.licdn.com/media/AAEAAQAAAAAAAAdcAAAAJDc2YjI2OGNmLWUwNWMtNDJjYy04NzA2LWNjM2I4NjAwMzUzOQ.jpg', '21', 'Unity Developer na CopLaps', 'JiuJitsu', 'Web Summit Volunteer'),
(3, 'joao', 'joao', 'Joao Batista', 'https://instagram.flis2-1.fna.fbcdn.net/t51.2885-15/e35/p480x480/14592040_1220527548021714_8677988512559005696_n.jpg?ig_cache_key=MTM5MTUzMTM0MzI2MzU3NDA0NA%3D%3D.2', '21', 'Estagiario Labpsicom', 'Falar com o PT', 'WebSubmit (com B)'),
(4, 'ana', 'ana', '', '', '', '', '', ''),
(5, 'zemanel', '12345', '', '', '', '', '', ''),
(6, 'soeiro', 'oproprio', 'O MAIOR DELES TODOS!!!', 'http://papinade.com/wp-content/uploads/2014/10/Mario-Balotelli-why-always-me.png', '21', 'Unity Developer na CopLaps', '', ''),
(7, 'joana', 'joana', 'asda', '', '', '', '', ''),
(10, 'manel', 'manel', 'manel', 'http://www.leyaonline.com/fotos/produtos/500_9789895553273_doces_manel.jpg', '60+', 'se tu soubesses', 'cristina', 'LGBT'),
(11, 'andre', 'andre', 'Andre Cerveira (JOLAS)', 'https://scontent.flis2-1.fna.fbcdn.net/v/t1.0-0/p206x206/12279064_10153779400963894_3074081350070489330_n.jpg?oh=084270ee9677b768158f9ee466bc47ed&oe=58E76580', '22', 'Sites de Wordpress para Joomla', 'WOW!', 'XIO MÃƒE');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `friendrequest`
--
ALTER TABLE `friendrequest`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `friendslist`
--
ALTER TABLE `friendslist`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `friendrequest`
--
ALTER TABLE `friendrequest`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `friendslist`
--
ALTER TABLE `friendslist`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
