<?php
session_start();
include 'connect.php';

function loggedin(){
    if(isset($_SESSION['uid']) && !empty($_SESSION['uid'])){
        return true;
    }else{
        return false;
    }
}

function getUsers($ID, $columnName){
    include 'connect.php'; //Notice: Undefined variable: pdo    &&   Fatal error: Call to a member function prepare() on null
    $query = $pdo->prepare("SELECT  `:columnName` FROM `users` WHERE ID=':ID'");
    $query->bindParam(':columnName', $columnName);
    $query->bindParam(':ID', $ID);
    $query->execute();    
    $queryArray = $query->fetch(PDO::FETCH_BOTH);
    return $queryArray[$columnName];
}

function informationAccess($access){
    if(isset($_GET['user']) && !empty($_GET['user'])){
        $user= $_GET['user'];
    }else{
        $user = $_SESSION['uid'];
    }

    if($access == true){
        $myID= $_SESSION['uid'];
        $username = getUsers($user,'username');
        $name = getUsers($user,'name');
        $profilePic = getUsers($user,'profilePic');
        $age = getUsers($user,'age');
        $professionalExperience = getUsers($user,'professionalExperience');
        $hobbies = getUsers($user,'hobbies');
        $volunteerExperience = getUsers($user,'volunteerExperience');
    }else{
        $myID= $_SESSION['uid'];
        $username = getUsers($user,'username');
        $name = getUsers($user,'name');
        $profilePic = getUsers($user,'profilePic');
    }    
}