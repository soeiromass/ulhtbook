<html>
<head>
    <title> Lista de Amigos - ULHTBOOK </title>
    <link rel='stylesheet' href ='style.css' />
</head>

<body>
<?php 
include 'connect.php'; 
include 'functions.php'; 
include 'header.php';
?>
<div class='container'>
    <h3>Pedidos de Amizade:</h3>
    <?php
    $myID= $_SESSION['uid'];
    $friendsQuery = $pdo->prepare("SELECT `firstUser`,`secondUser` FROM `friendslist` WHERE (`firstUser`=':myID' OR  `secondUser`=':myID')");
    $friendsQuery->bindParam(':myID', $myID);
    $friendsQuery->execute();
    while($friendsArray = $friendsQuery->fetch(PDO::FETCH_BOTH)){
        $userOne = $friendsArray['firstUser'];
        $userTwo = $friendsArray['secondUser'];
        if($userOne == $myID){
            $user = $userTwo;
        }else{
            $user = $userOne;
        }
        $username = getUsers($user, 'username');
        
        echo "<a href='profile.php?user=$user' class='box' style='display:block'>$username</a>";
    }
    ?>
</div>
</body>
</html>