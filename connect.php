<?php

$servername = "localhost";
$DBusername = "root";
$DBpassword = "";
$DBname = "ulhtbook";

try {
    $pdo = new PDO("mysql:host=$servername;dbname=$DBname", $DBusername, $DBpassword);
    // set the PDO error mode to exception
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    //echo "Connected! DONE!!";
    }
catch(PDOException $e)
    {
    echo "Error: " . $e->getMessage();
    }