<html>
<head>
    <title> ULHTBOOK Registo </title>
    <link rel='stylesheet' href ='style.css' />
</head>

<body>
<?php 
include 'connect.php';
include 'functions.php';
include 'header.php';
?>

<div class='container'>    
    <form method='post'>
        <?php
        $myID = $_SESSION['uid'];
        if($myID == '1'){
            if(isset($_POST['submit'])){
                $username= $_POST['username'];
                $password= $_POST['password'];
                $name= $_POST['name'];
                $profilePic= $_POST['profilePic'];
                $age= $_POST['age'];
                $professionalExperience= $_POST['professionalExperience'];
                $hobbies= $_POST['hobbies'];
                $volunteerExperience= $_POST['volunteerExperiences'];

                if (empty($username) || empty($password) || empty($name) || empty($profilePic) || empty($age) || empty($professionalExperience) || empty($hobbies) || empty($volunteerExperience)) {
                    $mensage= "Faltou preencher algum campo, por favor preencha todos os campos!";
                }
                else{
                    $registerQuerie = $pdo->prepare("INSERT INTO `users` (username, password, name, profilePic, age, professionalExperience, hobbies, volunteerExperience) 
                                                        VALUES (:username, :password, :name, :profilePic, :age, :professionalExperience, :hobbies, :volunteerExperiences)");
                    $registerQuerie->bindParam(':username', $username);
                    $registerQuerie->bindParam(':password', $password);
                    $registerQuerie->bindParam(':name', $name);
                    $registerQuerie->bindParam(':profilePic', $profilePic);
                    $registerQuerie->bindParam(':age', $age);
                    $registerQuerie->bindParam(':professionalExperience', $professionalExperience);
                    $registerQuerie->bindParam(':hobbies', $hobbies);
                    $registerQuerie->bindParam(':volunteerExperiences', $volunteerExperience);

                    $registerQuerie->execute();                    

                    $mensage="Registo feito com Sucesso. DONE!";        
                }
                echo "<div class='box'>$mensage</div>";
            }
                    
        ?>
        <h3>Registar Nova conta</h3>
        Nome do Utilizador:<br>
        <input type='text' name='username'/><br>        
        Palavra Chave: <br>
        <input type='password' name='password'/><br>
        Nome:<br>
        <input type='text' name='name'/><br>  
        Imagem de Perfil(URL):<br>
        <input type='text' name='profilePic'/><br>        
        Idade: <br>
        <input type='text' name='age'/><br>
        Experiencia Proficional:<br>
        <input type='text' name='professionalExperience'/><br>        
        Hobbies: <br>
        <input type='text' name='hobbies'/><br>
        Experiencia em Voluntariado: <br>
        <input type='text' name='volunteerExperiences'/><br>
        
        <input type='submit' name='submit' value='Registar'/>
        <?php
        }else{
            echo "<h1>Esta pagina apenas pode ser acedida pelo ADMIN!!</h1>";
        }
        ?>
    </form>
</div>
</body>
</html>