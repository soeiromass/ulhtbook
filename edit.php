<?php
include 'connect.php';
include 'functions.php';
include 'header.php';

$request = $_GET['request'];
$user = $_GET['user'];
$myID = $_SESSION['uid'];

if($request == 'edit'){
?>
<html>
<head>
    <title> Editar Perfil ULHTBOOK </title>
    <link rel='stylesheet' href ='style.css' />
</head>

<body>

<div class='container'>    
    <form method='post'>
        <?php
        if($myID == '1' || $myID == $user){
            if(isset($_POST['submit'])){
                $username= $_POST['username'];
                $password= $_POST['password'];
                $name= $_POST['name'];
                $profilePic= $_POST['profilePic'];
                $age= $_POST['age'];
                $professionalExperience= $_POST['professionalExperience'];
                $hobbies= $_POST['hobbies'];
                $volunteerExperience= $_POST['volunteerExperiences'];

                if (empty($username) || empty($password) || empty($name) || empty($profilePic) || empty($age) || empty($professionalExperience) || empty($hobbies) || empty($volunteerExperience)) {
                    $mensage= "Faltou preencher algum campo, por favor preencha todos os campos!";
                }
                else{
                    $editQuerie = $pdo->prepare("UPDATE  `users` SET username=':username', password=':password', name=':name', profilePic=':profilePic', age=':age', 
                                                                        professionalExperience=':professionalExperience', hobbies=':hobbies', volunteerExperience=':volunteerExperience' WHERE ID='$user'");
                    $editQuerie->bindParam(':username', $username);
                    $editQuerie->bindParam(':password', $password);
                    $editQuerie->bindParam(':name', $name);
                    $editQuerie->bindParam(':profilePic', $profilePic);
                    $editQuerie->bindParam(':age', $age);
                    $editQuerie->bindParam(':professionalExperience', $professionalExperience);
                    $editQuerie->bindParam(':hobbies', $hobbies);
                    $editQuerie->bindParam(':volunteerExperiences', $volunteerExperiences);
                    $editQuerie->execute();                    

                    $mensage="Registo feito com Sucesso. DONE!";        
                }
                echo "<div class='box'>$mensage</div>";
            }
                    
        ?>
        <h3>Registar Nova conta</h3>
        Nome do Utilizador:<br>
        <input type='text' name='username'/><br>        
        Palavra Chave: <br>
        <input type='password' name='password'/><br>
        Nome:<br>
        <input type='text' name='name'/><br>  
        Imagem de Perfil(URL):<br>
        <input type='text' name='profilePic'/><br>        
        Idade: <br>
        <input type='text' name='age'/><br>
        Experiencia Proficional:<br>
        <input type='text' name='professionalExperience'/><br>        
        Hobbies: <br>
        <input type='text' name='hobbies'/><br>
        Experiencia em Voluntariado: <br>
        <input type='text' name='volunteerExperiences'/><br>
        
        <input type='submit' name='submit' value='Registar'/>
        <?php
        }else{
            echo "<h1>Esta pagina apenas pode ser acedida pelo ADMIN!!</h1>";
        }
        ?>
    </form>
</div>
</body>
</html>
<?php
//header('location: profile.php?user='.$user);
}
if($request == 'delete'){    
    $deleteUser = $pdo->prepare("DELETE FROM `users` WHERE `ID`='$user'");
    $deleteUser->execute();
    header('location: members.php?user='.$user);
}