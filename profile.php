<html>
<head>
    <title> Prefile - ULHTBOOK </title>
    <link rel='stylesheet' href ='style.css' />
</head>

<body>
<?php 
include 'connect.php'; 
include 'functions.php'; 
include 'header.php';
?>
<div class='container'>
<?php
if(isset($_GET['user']) && !empty($_GET['user'])){
    $user= $_GET['user'];
}else{
    $user = $_SESSION['uid'];
}

$myID= $_SESSION['uid'];
$username = getUsers($user,'username');
$name = getUsers($user,'name');
$profilePic = getUsers($user,'profilePic');
$age = getUsers($user,'age');
$professionalExperience = getUsers($user,'professionalExperience');
$hobbies = getUsers($user,'hobbies');
$volunteerExperience = getUsers($user,'volunteerExperience'); 
   
?>
<h2><b><?php echo $username; ?></b></h2>
<h4><b>Nome:</b> <?php echo $name; ?></h4>
<h4><b>Idade:</b> <?php echo $age; ?></h4> 
<h4><b>Foto de Perfil:</b> <img src="<?php echo $profilePic; ?>"></h4>
<?php 
if($user != $myID){
    $friendship = $pdo->prepare("SELECT COUNT(`ID`) FROM `friendslist` WHERE (firstUser=':myID' AND secondUser=':myID') OR (firstUser=':myID' AND secondUser=':myID')");
    $friendship->bindParam(':myID', $myID);
    $friendship->bindParam(':user', $user);
    $friendship->execute();
    $count = $friendship->fetchColumn();
    if($count== 1){  
        echo "<h4><b>Experiência Profissional:</b> $professionalExperience</h4>";
        echo "<h4><b>Hobbies:</b>$hobbies</h4>";
        echo "<h4><b>Voluntariado em:</b>$volunteerExperience</h4>";        
        if($count == 1 ){
            echo "<h4><b>O Utilizador $username</b> é teu Amigo!</b></h4>";
            echo "<a href='friendsRequest.php?request=unfriend&user=$user' class='box'> Remover Amizade com $username</a>";
        }        
    }if($count != 1){
        $requestReceveQuery = $pdo->prepare("SELECT COUNT(`ID`) FROM `friendrequest` WHERE (`from`=':user' AND `to`=':myID')");
        $requestSendQuery = $pdo->prepare("SELECT COUNT(`ID`) FROM `friendrequest` WHERE (`from`=':myID' AND `to`=':user')");
        $requestReceveQuery->bindParam(':myID', $myID);
        $requestReceveQuery->bindParam(':user', $user);
        $requestSendQuery->bindParam(':myID', $myID);
        $requestSendQuery->bindParam(':user', $user);
        $requestReceveQuery->execute();
        $requestSendQuery->execute();
        $reciveRequestCount = $requestReceveQuery->fetchColumn();
        $sendRequestCount = $requestSendQuery->fetchColumn();
        if($reciveRequestCount == 1 || $sendRequestCount == 1){
            if($reciveRequestCount == 1){
                echo "<a href='friendsRequest.php?request=decline&user=$user' class='box'>Recusar Pedido!</a> <a href='friendsRequest.php?request=accept&user=$user' class='box'> Aceitar Pedido de $username</a>";
            }
            if($sendRequestCount == 1){
                echo "<a href='friendsRequest.php?request=cancel&user=$user' class='box'>Cancelar Pedido!</a>";
            }
        }else{
            if($myID != '1'){
                echo "<h2>O acesso a esta informação é limitada apenas a Amigos!</h2>";
            }            
            echo "<a href='friendsRequest.php?request=send&user=$user' class='box'>Enviar Pedido de Amizade</a>";
            echo "  </p> </p>"; // estes espaço é para o botão não ficar colado ao background
        }

    }    
}
if($user == $myID){    
    echo "<h4><b>Experiência Profissional: </b>$professionalExperience</h4>";
    echo "<h4><b>Hobbies: </b>$hobbies</h4>";
    echo "<h4><b>Voluntariado em: </b>$volunteerExperience</h4>";
    echo "<h2>Bem Vindo <b>$username</b></h2>";
    echo "<a href='edit.php?request=edit&user=$user' class='box'>Editar Perfil</a>";
    if($myID != '1'){
        echo "  </p> </p>";
    }   
}
if($myID == '1' && $myID != $user){
    echo "  <a href='edit.php?request=edit&user=$user' class='box'>Editar Perfil de $username</a>";
    echo "  <a href='edit.php?request=delete&user=$user' class='box'>Apagar Perfil de $username</a>";
    echo "  </p> </p>";
}
?>   
</div>
</body>
</html>