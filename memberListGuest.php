<html>
<head>
    <title> Lista de Membros (Guest) - ULHTBOOK </title>
    <link rel='stylesheet' href ='style.css' />
</head>

<body>
<?php 
include 'connect.php'; 
include 'functions.php'; 
include 'header.php';
?>
<div class='container'>
    <h3>Utilizadores:</h3>
    <?php
        $usersQuery = $pdo->prepare("SELECT ID FROM `users`");
        $usersQuery->execute();
        while($usersArray = $usersQuery->fetch(PDO::FETCH_BOTH)){
            $uid = $usersArray['ID'];
            $username = getUsers($uid, 'username');
            //echo $username; ### apenas para ver se os users estão a funcionar ###
            echo "<a href='profileForGuests.php?user=$uid' class='box' style='display:block'>$username</a>";
        }
    ?>   
</div>
</body>
</html>