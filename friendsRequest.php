<?php
include 'connect.php';
include 'functions.php';

$request = $_GET['request'];
$user = $_GET['user'];
$myID = $_SESSION['uid'];

if($request == 'send'){    
    $sendRequest = $pdo->prepare("INSERT INTO `friendRequest` (`from`, `to`) VALUES (:from, :to)"); //não pode ter `` no :from nem no :to
    $sendRequest->execute(array(':from' => $myID, ':to' => $user));
}

if($request == 'cancel'){
    $cancelRequest = $pdo->prepare("DELETE FROM `friendRequest` WHERE `from`=':myID' AND `to`=':user'");
    $cancelRequest->bindParam(':myID', $myID);
    $cancelRequest->bindParam(':user', $user);
    $cancelRequest->execute();
}

if($request == 'accept'){
    $deleteRequest = $pdo->prepare("DELETE FROM `friendRequest` WHERE `from`=':user' AND `to`=':myID'");
    $newFriendship = $pdo->prepare("INSERT INTO `friendslist` (`firstUser`, `secondUser`) VALUES (':user', ':myID')");
    $deleteRequest->bindParam(':myID', $myID);
    $deleteRequest->bindParam(':user', $user);
    $newFriendship->bindParam(':myID', $myID);
    $newFriendship->bindParam(':user', $user);
    $deleteRequest->execute();
    $newFriendship->execute();
}
if($request == 'decline'){
    $declineRequest = $pdo->prepare("DELETE FROM `friendRequest` WHERE `from`=':user' AND `to`=':myID'");
    $declineRequest->bindParam(':myID', $myID);
    $declineRequest->bindParam(':user', $user);
    $deleteRequest->execute();
}
if($request == 'unfriend'){
    $deleteFriendship = $pdo->prepare("DELETE FROM `friendslist` WHERE (`firstUser`= ':user' AND `secondUser`=':myID') OR (`firstUser`= ':myID' AND `secondUser`=':user')");
    $declineRequest->bindParam(':myID', $myID);
    $declineRequest->bindParam(':user', $user);
    $deleteFriendship->execute();
}

header('location: profile.php?user='.$user);